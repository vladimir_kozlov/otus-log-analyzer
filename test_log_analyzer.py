import unittest
from datetime import datetime
from pathlib import Path

import log_analyzer


class LogFilenameTest(unittest.TestCase):
    def test_get_last_log(self):
        log_last = log_analyzer.get_last_log("./test_data/test_get_last_log")
        self.assertEqual(
            log_last,
            log_analyzer.LogFile(
                path=Path("./test_data/test_get_last_log/nginx-access-ui.log-20170701.gz"),
                date=datetime(year=2017, month=7, day=1),
                format="gzip"
            )
        )


class LogFileTest(unittest.TestCase):
    def run_test_parse_log_line(self, log_file: log_analyzer.LogFile):
        lines = [log_analyzer.parse_log_line(line) for line in log_analyzer.read_log_file(log_file)]
        self.assertEqual(len(lines), 3)
        self.assertIsNone(lines[1])  # lines cannot be parsed
        self.assertIsNone(lines[2])
        self.assertEqual(
            lines[0],
            log_analyzer.LogLineData(
                request_url="/api/v2/banner/25019354",
                request_time=0.
            )
        )
        self.assertEqual(
            log_analyzer.calc_log_stats(lines, report_size=1, parse_error_threshold=1.),
            {
                "/api/v2/banner/25019354": log_analyzer.LogUrlStats(
                    count=1, count_perc=100.,
                    time_sum=0., time_perc=0., time_avg=0., time_max=0., time_med=0.
                )
            }
        )
        with self.assertRaises(ValueError):
            # 2 lines out of 3 with error, should raise an exception
            log_analyzer.calc_log_stats(lines, report_size=1, parse_error_threshold=0.66)

    def test_parse_log_line_plain(self):
        log_file = log_analyzer.LogFile(
            path=Path("./test_data/test_read_log_file/log"),
            date=datetime(year=1970, month=1, day=1),
            format="plain"
        )
        self.run_test_parse_log_line(log_file)

    def test_parse_log_line_gzip(self):
        log_file = log_analyzer.LogFile(
            path=Path("./test_data/test_read_log_file/log.gz"),
            date=datetime(year=1970, month=1, day=1),
            format="gzip"
        )
        self.run_test_parse_log_line(log_file)

    def test_parse_log_empty(self):
        log_file = log_analyzer.LogFile(
            path=Path("./test_data/test_read_log_file/log_empty"),
            date=datetime(year=1970, month=1, day=1),
            format="plain"
        )
        lines = [log_analyzer.parse_log_line(line) for line in log_analyzer.read_log_file(log_file)]
        self.assertEqual(len(lines), 0)
        self.assertEqual(
            log_analyzer.calc_log_stats(lines, report_size=1, parse_error_threshold=1.),
            {}
        )


class ReportTest(unittest.TestCase):
    def test_check_report_exists(self):
        self.assertTrue(log_analyzer.check_report_exists('report-1970.01.01.html', "./test_data/test_report"))
        self.assertFalse(log_analyzer.check_report_exists('report.html', "./test_data/test_report"))

    def test_format_report(self):
        log_file = log_analyzer.LogFile(
            path=Path("./test_data/test_report/log"),
            date=datetime(year=1970, month=1, day=1),
            format="plain"
        )
        report = log_analyzer.format_report(
            log_analyzer.calc_log_stats(
                map(
                    log_analyzer.parse_log_line,
                    log_analyzer.read_log_file(log_file)
                ),
                report_size=1,
                parse_error_threshold=1.
            ),
            "./report.html"
        )
        with open('./test_data/test_report/report-1970.01.01.html', 'r', encoding='utf-8') as fp:
            report_gt = fp.read()
        self.assertEqual(report, report_gt)


if __name__ == "__main__":
    unittest.main()
