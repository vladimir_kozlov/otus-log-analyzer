#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import gzip
import json
import logging
import os
import re
import sys
from datetime import datetime
from pathlib import Path
from statistics import mean, median
from string import Template
from typing import Iterable, Literal, NamedTuple, Optional, Union

# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';

LOG_LINE_FORMAT = re.compile(
    r"(?P<remote_addr>.*) (?P<remote_user>\S+)  (?P<http_x_real_ip>\S+) "
    r"\[(?P<time_local>.*)\] "
    r"\"(?P<method>\S+) (?P<request_uri>\S+) (?P<http_version>\S+)\" "
    r"(?P<status>\d+) (?P<body_bytes_sent>\d+) \"(?P<http_referer>.*)\" "
    r"\"(?P<http_user_agent>.*)\" \"(?P<http_x_forwarded_for>.*)\" "
    r"\"(?P<http_X_REQUEST_ID>.*)\" \"(?P<http_X_RB_USER>.*)\" (?P<request_time>\d+(\.\d*)?)"
)
LOG_FILENAME_FORMAT = re.compile(r"nginx-access-ui\.log-(?P<date>[0-9]{8})(?P<extension>\.(gz|txt|log)|)")


config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log"
}

logging.basicConfig(
    format="[%(asctime)s] %(levelname).1s %(message)s",
    datefmt="%Y.%m.%d %H:%M:%S",
    level=logging.INFO,
    filename=config.get("LOG_FILENAME")
)
logger = logging.getLogger()


class LogFile(NamedTuple):
    path: Path
    date: datetime
    format: Literal["gzip", "plain"]

    @property
    def name(self):
        return self.path.name


def get_last_log(log_dir: Union[str, os.PathLike]) -> Optional[LogFile]:
    """
    Find last log file in specified directory.
    File is considered a valid log file if:
        - it is a file (not directory)
        - its name conforms to format 'nginx-access-ui.log-<date: %Y%m%d>.<extension: txt (plain text) or gz (gzip)>'
    This function returns valid log file with latest date specified in filename, or None if no such file exist.

    :param log_dir: path to directory with logs
    :return:
        - None, if no valid log files exist in specified directory;
        - path to file, parsed datetime and parsed file format otherwise.
    """
    log_last: Optional[LogFile] = None

    for filename in Path(log_dir).iterdir():
        if filename.is_file():
            match = LOG_FILENAME_FORMAT.fullmatch(filename.name)
            if match is None:
                continue

            try:
                date = datetime.strptime(match.group("date"), "%Y%m%d")
            except ValueError:
                # this file is of no concern to us
                continue

            log = LogFile(
                path=filename,
                date=date,
                format='gzip' if match.group("extension").lstrip('.') == 'gz' else "plain"
            )
            if log is not None and (log_last is None or log_last.date < log.date):
                log_last = log

    return log_last


def read_log_file(log_file: LogFile) -> Iterable[str]:
    with (
        open(log_file.path, mode='r', encoding='utf-8')
        if log_file.format == 'plain'
        else gzip.open(log_file.path, mode='rt', encoding='utf-8')
    ) as fp:
        for line in fp:
            yield line.strip()


class LogLineData(NamedTuple):
    request_url: str
    request_time: float


def parse_log_line(log_line: str) -> Optional[LogLineData]:
    """
    Parse log line according to nginx format and extract request URL and request time data.

    :param log_line: line from log
    :return:
        - if line cannot be parsed, returns None
        - otherwise, returns request URL and request time from line
    """
    match = LOG_LINE_FORMAT.fullmatch(log_line)
    if match is None:
        return None
    request_url = match.group("request_uri")
    try:
        request_time = float(match.group("request_time"))
    except ValueError:
        return None

    return LogLineData(request_url=request_url, request_time=request_time)


class LogUrlStats(NamedTuple):
    count: int
    count_perc: float
    time_sum: float
    time_perc: float
    time_avg: float
    time_max: float
    time_med: float


def calc_log_stats(
    log_parsed: Iterable[Optional[LogLineData]],
    report_size: int,
    parse_error_threshold: float = 1.
) -> Optional[dict[str, LogUrlStats]]:
    log_data: dict[str, list[float]] = {}
    parse_error_count = 0
    line_count = 0
    for log_line in log_parsed:
        line_count += 1
        if log_line is None:
            parse_error_count += 1
        else:
            log_data.setdefault(log_line.request_url, []).append(log_line.request_time)
    if line_count > 0 and parse_error_count / line_count > parse_error_threshold:
        raise ValueError("PARSING_ERRORS", parse_error_count, line_count, parse_error_threshold)

    log_stats: dict[str, LogUrlStats] = {}
    request_count_total = sum(len(request_times) for request_times in log_data.values())
    request_time_total = sum(sum(request_times) for request_times in log_data.values())
    for request_url, request_times in log_data.items():
        log_stats[request_url] = LogUrlStats(
            count=len(request_times),
            count_perc=(len(request_times) / request_count_total) * 100,
            time_sum=sum(request_times),
            time_perc=(sum(request_times) / request_time_total if request_time_total > 0 else 0.) * 100,
            time_avg=mean(request_times),
            time_max=max(request_times),
            time_med=median(request_times)
        )
    log_stats = dict(
        sorted(
            log_stats.items(),
            key=lambda t: t[1].time_sum,
            reverse=True
        )[:report_size]
    )
    return log_stats


def format_report(log_stats: dict[str, LogUrlStats], path_to_report_template: Union[str, os.PathLike]) -> str:
    with open(path_to_report_template, 'r', encoding='utf-8') as fp:
        report_template = Template(fp.read())

    log_stats_formatted = [
        {
            "url": url,
            **log_url_stats._asdict()
        }
        for url, log_url_stats in log_stats.items()
    ]

    return report_template.safe_substitute(table_json=json.dumps(log_stats_formatted))


def check_report_exists(report_name: str, report_dir: Union[str, os.PathLike]) -> bool:
    report_dir = Path(report_dir)
    return (Path(report_dir) / report_name).exists()


def write_report(report: str, report_name: str, report_dir: Union[str, os.PathLike]):
    report_dir = Path(report_dir)
    report_dir.mkdir(parents=True, exist_ok=True)
    with open(Path(report_dir) / report_name, 'w', encoding='utf-8') as fp:
        fp.write(report)


def parse_args():
    parser = argparse.ArgumentParser(description="Parse and analyse nginx log")
    parser.add_argument("--config", default="./config.json", help="Path to config file (JSON), defaults to %(default)s")

    args = parser.parse_args()
    return args


class Config(NamedTuple):
    report_size: int
    report_dir: str
    log_dir: str
    parse_error_percentage: float
    log_filename: Optional[str] = None


def read_config(config_path: Optional[str] = None) -> Config:
    """
    Read global configuration parameters.
    If config_path is not specified, then default parameter values are used.
    If config_path is specified, then values from file are used,
    with parameters not specified in file taking default values.

    :param config_path: path to config file in JSON format or None.
    :return: Global configuration values
    """
    if config_path is not None:
        with open(config_path, 'r', encoding='utf-8') as fp:
            config_new = json.load(fp)
    else:
        config_new = {}
    configuration = {**config, **config_new}
    return Config(
        report_dir=configuration["REPORT_DIR"],
        log_dir=configuration["LOG_DIR"],
        report_size=configuration["REPORT_SIZE"],
        log_filename=configuration.get("LOG_FILENAME"),
        parse_error_percentage=configuration.get("PARSE_ERROR_PERCENTAGE", 100) / 100.
    )


def set_logging_to_file(logger: logging.Logger, log_filename: Union[str, os.PathLike]):
    """
    Replace current logger handler with file handler. Preserves format and logging level.

    :param logger: Logger
    :param log_filename: path to logfile
    :return: nothing
    """
    handler_current = logger.handlers[0]
    logger.removeHandler(handler_current)

    handler_new = logging.FileHandler(log_filename, mode='w')
    handler_new.setLevel(handler_current.level)
    handler_new.setFormatter(handler_current.formatter)

    logger.addHandler(handler_new)


def main():
    try:
        args = parse_args()

        logger.info("Retrieving configuration data...")
        config = read_config(args.config)
        logger.info("Done!")
        if config.log_filename is not None:
            set_logging_to_file(logger, config.log_filename)

        logger.info("Searching for last nginx log file...")
        log_last = get_last_log(config.log_dir)
        if log_last is None:
            logger.info("No nginx log found in %s, exiting...", config.log_dir)
            return
        logger.info("Found log file: %s", log_last.name)
        report_name = "report-{date}.html".format(date=log_last.date.strftime("%Y.%m.%d"))
        if check_report_exists(report_name, config.report_dir):
            logger.info("Log file %s already parsed to report %s, exiting...", log_last.name, report_name)
            return

        logger.info("Parsing nginx log file %s...", log_last.name)
        log_parsed = map(parse_log_line, read_log_file(log_last))
        try:
            log_stats = calc_log_stats(
                log_parsed,
                report_size=config.report_size,
                parse_error_threshold=config.parse_error_percentage
            )
        except ValueError as e:
            if e.args[0] == "PARSING_ERRORS":
                parse_error_count, line_count, parse_error_threshold = e.args[1:]
                logger.error(
                    (
                        "%d log lines out of total %d have been parsed with error, "
                        "which is higher than acceptable parsing error percentage of %.2f%%, "
                        "exiting..."
                    ),
                    parse_error_count, line_count, parse_error_threshold
                )
                sys.exit(1)
            else:
                raise
        if log_stats is None:
            return
        logger.info("Done!")

        logger.info("Writing report to %s...", report_name)
        report_str = format_report(log_stats, './report.html')
        write_report(report_str, report_name, config.report_dir)
        logger.info("Done!")
    except SystemExit:
        raise

    except BaseException:
        logger.exception("An error has occurred while running:")
        sys.exit(1)


if __name__ == "__main__":
    main()
